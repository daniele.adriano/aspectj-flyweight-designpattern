The flyweight design pattern revisited with AspectJ

For further details refer to document https://gitlab.com/daniele.adriano/aspectj-flyweight-designpattern/-/blob/master/flyweight.pdf