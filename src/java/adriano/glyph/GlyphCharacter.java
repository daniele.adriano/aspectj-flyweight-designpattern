package adriano.glyph;

import adriano.flyweight.ConcreteFlyweight;
import adriano.flyweight.IndexBasedContext;

/**
 * A Character. It is a Concrete flyweight, its intrinsic state is a character. The extrinsic state is the drawing
 * format (uppercase or lowercase), stored into a {@link IndexBasedContext}.
 */
public class GlyphCharacter implements Glyph {

    // Intrinsic state
    private char character;

    @ConcreteFlyweight
    public GlyphCharacter(char character) {
        this.character = character;
    }

    /**
     * Draw current character. Provided context is checked in order to apply the right drawing format (uppercase or lowercase)
     *
     * @param context extrinsic state
     */
    public void draw(IndexBasedContext<Boolean> context) {
        // Checks extrinsic state in order to apply right drawing format
        System.out.print(context.getState() ? Character.toUpperCase(character) : Character.toLowerCase(character));
    }

    public void insert(Glyph glyph, IndexBasedContext<Boolean> context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void first(IndexBasedContext<Boolean> context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void next(IndexBasedContext<Boolean> context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isDone(IndexBasedContext<Boolean> context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Glyph current(IndexBasedContext<Boolean> context) {
        throw new UnsupportedOperationException();
    }

}