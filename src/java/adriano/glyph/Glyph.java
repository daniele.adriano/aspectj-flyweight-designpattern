package adriano.glyph;

import adriano.flyweight.IndexBasedContext;

/**
 * Generic Glyph. It is the Flyweight composite interface.
 */
public interface Glyph {

    /**
     * Draw the glyph
     *
     * @param context flyweight context
     */
    void draw(IndexBasedContext<Boolean> context);

    /**
     * Insert the provided child glyph at the current position of current parent glyph
     *
     * @param gliph   glyph to be inserted
     * @param context flyweight context
     */
    void insert(Glyph gliph, IndexBasedContext<Boolean> context);

    /**
     * Rewind children glyph index of current parent glyph to the first position
     *
     * @param context flyweight context
     */
    void first(IndexBasedContext<Boolean> context);

    /**
     * Return the child glyph at current children glyph index position
     *
     * @param context flyweight context
     * @return the child glyph at current children glyph index position
     */
    Glyph current(IndexBasedContext<Boolean> context);

    /**
     * Increase children glyph index of current parent glyph
     *
     * @param context flyweight context
     */
    void next(IndexBasedContext<Boolean> context);

    /**
     * True if children glyph index reached the last position, false otherwise
     *
     * @param context flyweight context
     * @return True if children glyph index reached the last position, false otherwise
     */
    boolean isDone(IndexBasedContext<Boolean> context);

}