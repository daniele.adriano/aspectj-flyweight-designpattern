package adriano.glyph;

import adriano.flyweight.IndexBasedContext;

import java.util.LinkedList;

/**
 * A column. It is a Glyph composite element that holds Row children
 */
public class GlyphColumn implements Glyph {

    private int index = 0;
    private LinkedList<GlyphRow> rows = new LinkedList<>();

    /**
     * Draw rows and print a newline after each row.
     *
     * @param context flyweight context
     */
    public void draw(IndexBasedContext<Boolean> context) {
        for (int i = index; i < rows.size(); i++) {
            rows.get(i).draw(context);
            System.out.println("");
        }
    }

    /**
     * Insert glyph row at current index position
     *
     * @param glyph   a glyph row
     * @param context flyweight context
     */
    public void insert(Glyph glyph, IndexBasedContext<Boolean> context) {
        rows.add(index, (GlyphRow) glyph);
    }

    /**
     * Rewind the index at the beginning of current column
     *
     * @param context flyweight context
     */
    @Override
    public void first(IndexBasedContext<Boolean> context) {
        for (GlyphRow row : rows) {
            row.first(context);
        }
        index = 0;
    }

    /**
     * Retrieves current glyph row
     *
     * @param context flyweight context
     * @return current glyph row
     */
    @Override
    public Glyph current(IndexBasedContext<Boolean> context) {
        return rows.get(index);
    }

    /**
     * Increase the index to next glyph row
     *
     * @param context flyweight context
     */
    @Override
    public void next(IndexBasedContext<Boolean> context) {
        GlyphRow row = rows.get(index);
        while (!row.isDone(context)) {
            row.next(context);
        }
        index += 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDone(IndexBasedContext<Boolean> context) {
        return index > rows.size();
    }

}