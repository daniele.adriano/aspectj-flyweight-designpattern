package adriano.glyph;

import adriano.analyzer.Analyzed;
import adriano.flyweight.IndexBasedContext;

/**
 * This simple class show how to populate and manipulate the glyph composite structure.
 * The glyph composite is a java implementation of the flyweight study case shown in the book "Design Patterns,
 * Elements of Reusable Object-Oriented Software - authors: Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides".
 */
public class GlyphLauncher {

    @Analyzed
    public static void main(String[] args) {
        String[] rawRows = {"hi buddy,", "wanna drink something?"};

        IndexBasedContext<Boolean> context = new IndexBasedContext<>();
        GlyphColumn col = new GlyphColumn();
        for (int i = 0; i < rawRows.length; i++) {
            GlyphRow row = new GlyphRow();
            col.insert(row, context);
            col.next(context);

            // Build glyph row, composed by concrete flyweight glyph characters
            char[] rawCharacters = rawRows[i].toCharArray();
            for (int j = 0; j < rawCharacters.length; j++) {

                // Handles extrinsic state
                if (j == 0) {
                    // First character of each row uppercase
                    context.setState(true, 1);
                } else if (j == 1) {
                    // Remaining characters of current row lowercase
                    context.setState(false, rawCharacters.length - 1);
                }

                // GlyphCharacter is a concrete flyweight, instances are handled by AnnotationFlyweightFactoryAspect
                GlyphCharacter character = new GlyphCharacter(rawCharacters[j]);
                row.insert(character, context);
                row.next(context);
            }
        }

        // Rewind indexes and print out the composite
        col.first(context);
        col.draw(context);
        System.out.println();

        // Insert new characters at the beginning of first and second row.
        // Note that these characters inherit the same case of the first letter of the row (uppercase)
        col.first(context);
        Glyph r1 = col.current(context);
        populateRow("at the bar: ", (GlyphRow) r1, context);
        col.next(context);
        Glyph r2 = col.current(context);
        populateRow("my name is john, ", (GlyphRow) r2, context);

        // Rewind indexes and print out the composite
        col.first(context);
        col.draw(context);
    }

    /**
     * Insert provided string in provided row
     *
     * @param str     string to be inserted
     * @param row     row to be populated
     * @param context flyweight context
     */
    private static void populateRow(String str, GlyphRow row, IndexBasedContext<Boolean> context) {
        char[] rawCharacters = str.toCharArray();
        for (int j = 0; j < rawCharacters.length; j++) {
            GlyphCharacter character = new GlyphCharacter(rawCharacters[j]);
            row.insert(character, context);
            row.next(context);
        }
    }

}