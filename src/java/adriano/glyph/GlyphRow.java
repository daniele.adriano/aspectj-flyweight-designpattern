package adriano.glyph;

import adriano.flyweight.IndexBasedContext;

import java.util.LinkedList;

/**
 * A row. It is a Glyph composite element that holds Character children
 */
public class GlyphRow implements Glyph {

    private int index = 0;
    private LinkedList<GlyphCharacter> characters = new LinkedList<>();

    /**
     * Draw row children
     *
     * @param context flyweight context
     */
    public void draw(IndexBasedContext<Boolean> context) {
        for (int i = index; i < characters.size(); i++) {
            characters.get(i).draw(context);
            next(context);
        }
    }

    /**
     * Insert glyph character at current position
     *
     * @param glyph   a glyph character
     * @param context flyweight context
     */
    public void insert(Glyph glyph, IndexBasedContext<Boolean> context) {
        if (index < characters.size() - 1) {
            context.insert(1);
        }
        characters.add(index, (GlyphCharacter) glyph);
    }

    /**
     * Rewind the index at the beginning of current row
     *
     * @param context flyweight context
     */
    @Override
    public void first(IndexBasedContext<Boolean> context) {
        context.prev(index);
        index = 0;
    }

    /**
     * Retrieves current glyph character
     *
     * @param context flyweight context
     * @return current glyph character
     */
    @Override
    public Glyph current(IndexBasedContext<Boolean> context) {
        return characters.get(index);
    }

    /**
     * Increase the index to next glyph character
     *
     * @param context flyweight context
     */
    @Override
    public void next(IndexBasedContext<Boolean> context) {
        index += 1;
        context.next(1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDone(IndexBasedContext<Boolean> context) {
        return index > characters.size() - 1;
    }

}