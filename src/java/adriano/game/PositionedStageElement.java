package adriano.game;

import java.awt.*;
import java.io.IOException;

/**
 * Positioned stage element. Holds StageElement (concrete flyweight) extrinsic state (x and y coordinates)
 */
public class PositionedStageElement {

    private Integer x;
    private Integer y;
    private StageElement stageElement;

    public PositionedStageElement(Integer x, Integer y, StageElement stageElement) throws IOException {
        this.x = x;
        this.y = y;
        this.stageElement = stageElement;
    }

    /**
     * Draw StageElement at x,y coordinates
     *
     * @param graphics graphics
     */
    public void paint(Graphics graphics) {
        stageElement.paint(graphics, x, y);
    }

}
