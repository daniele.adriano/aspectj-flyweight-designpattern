package adriano.game;

import adriano.analyzer.Analyzed;

import javax.swing.*;
import java.io.IOException;

/**
 * Simple study case to simulate how a game stage can be created.
 */
public class GameLauncher {

    private final static Integer SPRITE_SIDE_SIZE = 50;
    private final static Integer STAGE_SIDE_SIZE_MULTIPLIER = 10;
    private final static Integer STAGE_SIDE_SIZE = SPRITE_SIDE_SIZE * STAGE_SIDE_SIZE_MULTIPLIER;

    public static void main(String[] args) throws IOException {
        GameLauncher game = new GameLauncher();
        game.run();
    }

    /**
     * Build and show a game stage
     *
     * @throws IOException
     */
    @Analyzed
    public void run() throws IOException {
        Stage stage = new Stage();

        // Add background
        for (int y = 0; y < STAGE_SIDE_SIZE; y += SPRITE_SIDE_SIZE) {
            for (int x = 0; x < STAGE_SIDE_SIZE; x += SPRITE_SIDE_SIZE) {
                PositionedStageElement pse = new PositionedStageElement(x, y, new StageElement("grass.png"));
                stage.addElement(pse);
            }
        }

        // Add flowers
        boolean add = true;
        for (int y = 0; y < STAGE_SIDE_SIZE; y += SPRITE_SIDE_SIZE) {
            for (int x = 0; x < STAGE_SIDE_SIZE; x += SPRITE_SIDE_SIZE) {
                if (add) {
                    PositionedStageElement pse = new PositionedStageElement(x, y, new StageElement("flower.png"));
                    stage.addElement(pse);
                }
                add = !add;
            }
            add = !add;
        }

        // Show created stage
        Runnable task = () -> {
            stage.setSize(STAGE_SIDE_SIZE, STAGE_SIDE_SIZE);
            stage.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            stage.setResizable(false);
            stage.setVisible(true);
        };
        task.run();
    }

}
