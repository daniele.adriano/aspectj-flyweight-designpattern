package adriano.game;

import adriano.flyweight.ConcreteFlyweight;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * A stage element. It is a concrete flyweight, its intrinsic state is a BufferedImage.
 * Can be decorated in order to add element functionality
 */
public class StageElement {

    private BufferedImage sprite;

    @ConcreteFlyweight
    public StageElement(String imageName) throws IOException {
        sprite = ImageIO.read(new File(imageName));
    }

    /**
     * Draw the sprite (intrinsic state) at x,y position (extrinsic states)
     *
     * @param graphics graphic
     * @param x        x coordinate
     * @param y        y coordinate
     */
    public void paint(Graphics graphics, Integer x, Integer y) {
        graphics.drawImage(sprite, x, y, null);
    }
}
