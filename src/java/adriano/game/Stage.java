package adriano.game;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A game stage. Holds stage elements to be handled.
 */
public class Stage extends JFrame {

    private List<PositionedStageElement> stageElements = new ArrayList<>();

    /**
     * Add specified element to the stage
     *
     * @param stageElement stage element to be added
     */
    public void addElement(PositionedStageElement stageElement) {
        stageElements.add(stageElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paint(Graphics graphics) {
        for (PositionedStageElement gi : stageElements) {
            gi.paint(graphics);
        }
    }

}
