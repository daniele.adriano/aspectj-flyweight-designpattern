package adriano.flyweight;

/**
 * Implement a generic flyweight factory by storing concrete flyweight instances.
 * Each class annotated with {@link ConcreteFlyweight} annotation will be stored by this flyweight factory.
 */
public aspect AnnotationFlyweightFactoryAspect extends AbstractFlyweightFactoryAspect {

    declare error :  call(@ConcreteFlyweight new()) :"Constructor with @ConcreteFlyweight annotation must have at least one argument";

    pointcut create(): call(@ConcreteFlyweight new(..));

}