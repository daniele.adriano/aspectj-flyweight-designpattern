package adriano.flyweight;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Holds concrete flyweight extrinsic states into a {@link TreeMap}.
 * Context index must be kept aligned with concrete flyweight children index.
 * <br>
 * Example. If state S1 is associated to context index X then all children with index between 0 and X are associated
 * to state X. If a new state S2 is associated to context index Y (where Y is a number greater than X) then all children
 * between X and Y are associated to state Y.
 *
 *
 * @param <S> Context state class
 */
public class IndexBasedContext<S> {

    private Integer index = 0;
    private TreeMap<Integer, S> extrinsicStates = new TreeMap<>();

    /**
     * Insert elements at current index position. If current index value is greater than the greater TreeMap key
     * then nothing is done. Otherwise keys greater than specified quantity are shifted up by specified quantity value.
     *
     * @param quantity
     */
    public void insert(int quantity) {
        NavigableMap<Integer, S> tm = extrinsicStates.tailMap(index, true);
        List<Integer> toShiftUp = tm.keySet().stream().collect(Collectors.toList());
        for (int i = toShiftUp.size() - 1; i >= 0; i--) {
            Integer key = toShiftUp.get(i);
            S removed = extrinsicStates.remove(key);
            extrinsicStates.put(key + quantity, removed);
        }
    }

    /**
     * Rewind the index to the first position
     */
    public void first() {
        index = 0;
    }

    /**
     * Increase the index by the specified value
     *
     * @param step value to increase
     */
    public void next(int step) {
        index += step;
    }

    /**
     * Decrease the index by the specified value. The index cannot be negative.
     *
     * @param step value to decrease
     */
    public void prev(int step) {
        index -= step;
        if (index < 0) {
            throw new IndexOutOfBoundsException("Index cannot be negative");
        }
    }

    /**
     * Retrieve context state at the current position. As states are stored into a TreeMap, returned state is the
     * ceiling entry of the current index value
     *
     * @return context state at the current position
     */
    public S getState() {
        Map.Entry<Integer, S> entry = extrinsicStates.ceilingEntry(index);
        return entry != null ? entry.getValue() : null;
    }

    /**
     * Set a context state at the current position
     *
     * @param state
     * @param span
     */
    public void setState(S state, int span) {
        extrinsicStates.put(index + span - 1, state);
    }

    /**
     * Clear context state and rewind the index
     */
    public void clear() {
        extrinsicStates.clear();
        first();
    }

}
