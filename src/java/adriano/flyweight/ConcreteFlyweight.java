package adriano.flyweight;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


/**
 * Constructor marked with this annotation will be handled by {@link AnnotationFlyweightFactoryAspect} flyweight factory
 */
@Target(ElementType.CONSTRUCTOR)
public @interface ConcreteFlyweight {
}
