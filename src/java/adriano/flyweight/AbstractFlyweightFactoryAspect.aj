package adriano.flyweight;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implement a generic flyweight factory by storing concrete flyweight instances.
 */
public abstract aspect AbstractFlyweightFactoryAspect {

    // Stores concrete flyweight instances for each concrete flyweight type.
    // The inner hashtable holds concrete flyweight key and concrete flyweight instance.
    private Map<Class, Hashtable<Object, Object>> flyweightPool = new HashMap();

    // To synchronize access to the flyweight pool
    private ReentrantLock lock = new ReentrantLock();

    // Override to customize call() pointcut
    abstract pointcut create();

    pointcut exclude(): !within(AbstractFlyweightFactoryAspect+);

    /**
     * Handles flyweight factory instance pool.
     * The default concrete flyweight key is the first constructor argument.
     */
    Object around(): create() && exclude() {
        // Handles per class flyweight pool
        Class clz = thisJoinPointStaticPart.getSignature().getDeclaringType();
        lock.lock();
        try {
            if (!flyweightPool.containsKey(clz)) {
                flyweightPool.put(clz, new Hashtable<>());
            }
            Hashtable<Object, Object> perClzPool = flyweightPool.get(clz);

            // Handles concrete flyweight instances
            Object[] args = thisJoinPoint.getArgs();
            Object key = buildFlyweightKey(args);
            if (perClzPool.containsKey(key)) {
                return perClzPool.get(key);
            } else {
                Object flyweight = proceed();
                perClzPool.put(key, flyweight);
                return flyweight;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Retrieves concrete flyweight key. Default implementation retrieve the first constructor argument.
     * Override to customize concrete flyweight key.
     * @param args constructor arguments
     * @return concrete flyweight key
     */
    protected Object buildFlyweightKey(Object[] args) {
        return args[0];
    }

}