package adriano.morse;

import java.util.ArrayList;
import java.util.List;

import static adriano.morse.CodeConstants.BETWEEN_CHARACTERS_PAUSE;
import static adriano.morse.CodeConstants.BETWEEN_TONES_PAUSE;

/**
 * Created by daniele on 25/02/2018.
 */
public class MorseWord implements Morse {

    private List<MorseCharacter> characters = new ArrayList<>();

    @Override
    public void play(SoundPlayer soundPlayer, Integer volume, Integer speedMultiplier) {
        for (MorseCharacter sc : characters) {
            sc.play(soundPlayer, volume, speedMultiplier);
            soundPlayer.pause(BETWEEN_CHARACTERS_PAUSE - BETWEEN_TONES_PAUSE, speedMultiplier);
        }
    }

    @Override
    public void insert(Morse sound) {
        characters.add((MorseCharacter) sound);
    }
}
