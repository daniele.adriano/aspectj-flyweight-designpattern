package adriano.morse;

import javax.sound.midi.*;

/**
 * Created by daniele on 25/02/2018.
 */
public class SoundPlayer {

    public Synthesizer midiSynth;
    private MidiChannel midiChannel;

    public SoundPlayer() throws MidiUnavailableException {
        midiSynth = MidiSystem.getSynthesizer();
        Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
        midiSynth.loadInstrument(instr[0]);
        MidiChannel[] mChannels = midiSynth.getChannels();
        midiChannel = mChannels[0];
    }


    public void play(Integer note, Integer volume, Integer duration, Integer speedMultiplier, String label) {
        System.out.print(label);
        midiChannel.noteOn(note, volume);
        pause(duration, speedMultiplier);
        midiChannel.noteOff(note);
    }

    public void pause(Integer duration, Integer speedMultiplier) {
        try {
            Thread.sleep(duration / speedMultiplier);
        } catch (InterruptedException e) {
        }
    }

    public void turnOn() throws MidiUnavailableException {
        midiSynth.open();
    }

    public void turnOff() {
        midiSynth.close();
    }

}
