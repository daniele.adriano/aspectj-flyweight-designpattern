package adriano.morse;

import static adriano.morse.CodeConstants.DASH_DURATION;
import static adriano.morse.CodeConstants.DOT_DURATION;

/**
 * Created by daniele on 25/02/2018.
 */
public enum CodeTone {

    DOT('.', DOT_DURATION),
    DASH('_', DASH_DURATION);

    private Character character;
    private Integer duration;

    CodeTone(Character character, Integer duration) {
        this.character = character;
        this.duration = duration;
    }

    public Integer getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return character.toString();
    }

}
