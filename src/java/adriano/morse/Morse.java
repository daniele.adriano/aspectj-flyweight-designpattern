package adriano.morse;

/**
 * Created by daniele on 25/02/2018.
 */
public interface Morse {

    void play(SoundPlayer soundPlayer, Integer volume, Integer speedMultiplier);

    void insert(Morse sound);

}
