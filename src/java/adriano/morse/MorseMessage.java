package adriano.morse;

import java.util.ArrayList;
import java.util.List;

import static adriano.morse.CodeConstants.*;

/**
 * Created by daniele on 25/02/2018.
 */
public class MorseMessage implements Morse {

    private List<MorseWord> words = new ArrayList<>();

    @Override
    public void play(SoundPlayer soundPlayer, Integer volume, Integer speedMultiplier) {
        for (MorseWord w : words) {
            w.play(soundPlayer, volume, speedMultiplier);
            soundPlayer.pause(BETWEEN_WORDS_PAUSE - BETWEEN_CHARACTERS_PAUSE - BETWEEN_TONES_PAUSE, speedMultiplier);
        }
    }

    @Override
    public void insert(Morse sound) {
        words.add((MorseWord) sound);
    }
}
