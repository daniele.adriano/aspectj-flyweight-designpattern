package adriano.morse;

import java.util.ArrayList;
import java.util.List;

import static adriano.morse.CodeConstants.BETWEEN_TONES_PAUSE;

/**
 * Created by daniele on 25/02/2018.
 */
public class MorseCharacter implements Morse {

    List<MorseTone> codes = new ArrayList<>();

    private Character character;

    public MorseCharacter(Character character) {
        this.character = character;
    }

    @Override
    public void play(SoundPlayer soundPlayer, Integer volume, Integer speedMultiplier) {
        for (MorseTone sc : codes) {
            sc.play(soundPlayer, volume, speedMultiplier);
            soundPlayer.pause(BETWEEN_TONES_PAUSE, speedMultiplier);
        }
    }

    @Override
    public void insert(Morse sound) {
        codes.add((MorseTone) sound);
    }
}
