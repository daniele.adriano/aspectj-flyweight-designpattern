package adriano.morse;


import static adriano.morse.CodeTone.DASH;
import static adriano.morse.CodeTone.DOT;

/**
 * Created by daniele on 25/02/2018.
 */
public enum CodeEncoder {

    A(DOT, DASH),
    B(DASH, DOT, DOT, DOT);

    private CodeTone[] encoded;

    CodeEncoder(CodeTone... code) {
        this.encoded = code;
    }

    public CodeTone[] getMorseCode() {
        return encoded;
    }

}
