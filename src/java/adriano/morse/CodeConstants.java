package adriano.morse;

/**
 * Created by daniele on 25/02/2018.
 */
public class CodeConstants {

    public final static Integer DOT_DURATION = 250;

    public final static Integer DASH_DURATION = DOT_DURATION * 3;

    public final static Integer BETWEEN_TONES_PAUSE = DOT_DURATION;

    public final static Integer BETWEEN_CHARACTERS_PAUSE = DOT_DURATION * 3;

    public final static Integer BETWEEN_WORDS_PAUSE = DOT_DURATION * 7;

    public final static Integer NOTE_TONALITY = 80;


}
