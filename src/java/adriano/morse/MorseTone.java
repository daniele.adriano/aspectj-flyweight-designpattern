package adriano.morse;

import static adriano.morse.CodeConstants.NOTE_TONALITY;

/**
 * Created by daniele on 25/02/2018.
 */
//@ConcreteFlyweight
public class MorseTone implements Morse {

    private CodeTone tone;

    public MorseTone(CodeTone tone) {
        this.tone = tone;
    }

    @Override
    public void play(SoundPlayer soundPlayer, Integer volume, Integer speedMultiplier) {
        soundPlayer.play(NOTE_TONALITY, volume, tone.getDuration(), speedMultiplier, tone.toString());
    }

    @Override
    public void insert(Morse sound) {
        // do nothing
    }
}
