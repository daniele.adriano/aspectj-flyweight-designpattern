package adriano.morse;

import javax.sound.midi.MidiUnavailableException;

/**
 * Created by daniele on 24/02/2018.
 */
public class MainMorse {

    public static void main(String[] args) throws MidiUnavailableException {
        String message = "AAA";

        // Build composite
        MorseMessage mMessage = new MorseMessage();
        MorseWord mWord = new MorseWord();
        char[] chars = message.trim().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            Character c = Character.toUpperCase(chars[i]);
            if (c == ' ') {
                mMessage.insert(mWord);
                mWord = new MorseWord();
                continue;
            }
            MorseCharacter mCharacter = new MorseCharacter(c);
            CodeTone[] characterCode = CodeEncoder.valueOf(c.toString()).getMorseCode();
            for (int j = 0; j < characterCode.length; j++) {
                MorseTone mTone = new MorseTone(characterCode[j]);
                mCharacter.insert(mTone);
            }
            mWord.insert(mCharacter);
        }
        mMessage.insert(mWord);

        SoundPlayer sp = new SoundPlayer();
        sp.turnOn();
        try {
            // Play sound
            mMessage.play(sp, 100, 2);
        } finally {
            sp.turnOff();
        }
    }
}

