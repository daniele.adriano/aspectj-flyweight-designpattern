package adriano.analyzer;

/**
 * Analyze the number of class instances created and the memory used by methods annotated with {@link Analyzed} annotation
 */
public aspect AnnotationInstanceAnalyzerAspect extends AbstractInstanceAnalyzerAspect {

    pointcut analyze(): execution(@Analyzed * * (..));

}
