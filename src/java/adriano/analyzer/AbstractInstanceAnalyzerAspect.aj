package adriano.analyzer;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Analyze the number of class instances created and the memory used by specified pointcut. The amount of memory is
 * calculated checking the free memory before and after method execution.<br>
 * Override abstract pointcut analyze() to customize methods to be analyzed.<br>
 * Override void log() method to customize log output source (default is System.out)
 */
public abstract aspect AbstractInstanceAnalyzerAspect {

    // Stores the number of instance created for each analyzed class
    // TreeMap because keys are sorted
    private Map<String, Integer> instances = new TreeMap<>();

    // Synchronize pool access
    private ReentrantLock lock = new ReentrantLock();

    // Override to customize method to be analyzed
    abstract pointcut analyze();

    pointcut create(Object obj): execution(new(..)) && this(obj);

    pointcut createIn(): cflowbelow(analyze());

    pointcut exclude(): !within(AbstractInstanceAnalyzerAspect+);

    /**
     * Handle number of analyzed instances
     */
    after(Object obj): create(obj) && createIn() && exclude() {
        lock.lock();
        try {
            String clzName = obj.getClass().getName();
            if (!instances.containsKey(clzName)) {
                instances.put(clzName, 0);
            }
            Integer num = instances.get(clzName);
            instances.put(clzName, num + 1);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Log and cleanup collected data
     */
    Object around(): analyze() && exclude() {
        lock.lock();
        try {
            // Free memory before method execution
            Runtime runtime = Runtime.getRuntime();
            long initFreeMemory = runtime.freeMemory();

            Object reg = proceed();

            // Calculate used memory
            long memory = initFreeMemory - runtime.freeMemory();

            // Print out data
            log(String.format("----%-57s", "ANALYZER").replace(" ", "-"));
            log(format("Classes", "Instances"));
            for (Map.Entry<String, Integer> entry : instances.entrySet()) {
                log(format(entry.getKey(), String.valueOf(entry.getValue())));
            }
            log("");
            log(format("Memory used [KBytes]", String.valueOf(memory / 1024)));
            log(String.format("%61s", "-").replace(" ", "-"));

            instances.clear();
            return reg;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Override to use logger instead of System.out
     * @param str string to log
     */
    protected void log(String str) {
        System.out.println(str);
    }

    /**
     * Common method to format elements
     * @param left left element
     * @param right right element
     * @return formatted string
     */
    private String format(String left, String right) {
        return String.format("%50s %10s", left, right);
    }

}
