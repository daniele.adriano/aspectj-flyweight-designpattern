package adriano.analyzer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Methods marked with this annotation will be analyzed by {@link AnnotationInstanceAnalyzerAspect}
 */
@Target(ElementType.METHOD)
public @interface Analyzed {
}
