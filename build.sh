#!/bin/sh

###############################################################################
## Usage

# ./build.sh            Build the project without exclusions
# ./build.sh -nofw      Build the project excluding flyweight factory aspects


###############################################################################
## Script configurations

src_dir="src/java"
res_dir="src/resources"
out_dir="target"
clzpath="$CLASSPATH"
source_version="1.8"


###############################################################################
## Clean and compile

printf "\nCleaning target directory...\n"

rm -rf $out_dir
mkdir $out_dir

printf "Compiling sources..."

# Collect sources to be compiled
find $src_dir -name *.java > sources.txt
if [ "$1" = "-nofw" ]; then
    printf "excluding flyweight factory aspect sources..."
    find $src_dir -name *InstanceAnalyzerAspect.aj >> sources.txt
else
    find $src_dir -name *.aj >> sources.txt
fi

# Compile
ajc -$source_version -classpath $clzpath -d $out_dir @sources.txt

# Check compile command exit code and finalize the build
if [ $? == 0 ]; then
    printf "\nCopying resources...\n"
    cp $res_dir/* $out_dir

    printf "Done!\n"
else
    # Clean target directoy
    rm -rf $out_dir
    mkdir $out_dir
fi

rm sources.txt